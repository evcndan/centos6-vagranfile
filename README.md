# Un fichero Vagrantfile para la asignatura "Computación en la Nube" del Master de Ingeniería Informática de la Universidad de Las Palmas de Gran Canaria

Este fichero ```Vagrantfile``` define una máquina virtual (MV) para VirtualBox con un CentOS 6 instalado y varios paquetes...

* docker, docker-compose, docker-machine
* git, nodejs
* rvm (ruby version manager)
* Intefaz Gráfica de Usuario (Escritorio Gnome)

## Prerrequisitos
VirtuaBox, Vagrant e importante para que la interfaz gráfica funcione es necesario instalar el plugin "vguest", tal que así

```
$ vagrant plugin install vagrant-vbguest
```

## Encendido de la máquina virtual
A partir del repositorio obtenemos el fichero Vagrantfile (sólo una vez)

```
$ git clone https://bitbucket.org/evcndan/centos6-vagranfile.git
$ cd centos6-vagranfile
```

Y endendemos la máquina Vagrant cualquier MV

```
$ vagrant up
```

El directorio local es montado en ```/home/vagrant/sync``` de la MV.

### Autores
Carmelo Cuenca y Francisca Quintana 
