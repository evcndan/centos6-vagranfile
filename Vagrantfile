# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "centos/6"

  config.ssh.insert_key = false

  # Share an additional folder to the guest VM.
  config.vm.synced_folder '.', '/vagrant', disabled: true
  config.vm.synced_folder ".", "/home/vagrant/sync", type: "virtualbox"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant.
  config.vm.provider "virtualbox" do |vb|
  # Display the VirtualBox GUI when booting the machine
    vb.gui = true
  
    # Customize the amount of memory on the VM:
    vb.memory = "2048"
  end
 
  # Enable provisioning with a shell script.
  config.vm.provision "shell", inline: <<-SHELL
    yum update -y

    # spanish
    sed -i 's/LANG="en_US.UTF-8"/LANG="es_ES.UTF-8"/' /etc/sysconfig/i18n
    yum groupinstall -y "Soporte para español [es]"
    
    # some packages
    yum install -y patch libyaml-devel autoconf gcc-c++ patch readline-devel \
      zlib-devel libffi-devel openssl-devel automake libtool bison sqlite-devel

    # docker
    rpm -iUvh \
      http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
    yum update -y
    yum install -y docker-io
    groupadd docker && usermod -aG docker vagrant
    service docker start && chkconfig docker on
    
    
    # docker-compose
    URL=https://github.com/docker/compose/releases/download/1.11.2
    curl -L ${URL}/docker-compose-`uname -s`-`uname -m` > \
      /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
    
    # AWS CLI
    yum install -y python-pip
    pip install --upgrade pip
    pip install --upgrade --user awscli

    # RVM
    runuser -l vagrant -c \
      'gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3'
    runuser -l vagrant -c '\\curl -sSL https://get.rvm.io | bash'
    runuser -l vagrant -c 'rvm install 2.2.4'

    # git
    yum install -y git
    # nodejs
    sudo yum -y install nodejs npm

    # GUI
    yum groupinstall -y "Desktop" "Desktop Platform" "X Window System" "Fonts"
    
    init 5
  SHELL
end
